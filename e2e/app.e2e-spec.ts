import { MerklehashtreeUiPage } from './app.po';

describe('merklehashtree-ui App', () => {
  let page: MerklehashtreeUiPage;

  beforeEach(() => {
    page = new MerklehashtreeUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
