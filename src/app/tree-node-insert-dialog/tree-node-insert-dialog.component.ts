import { Component, OnInit } from '@angular/core';
import {MdDialogRef} from '@angular/material';

@Component({
  selector: 'app-tree-node-insert-dialog',
  templateUrl: './tree-node-insert-dialog.component.html',
  styleUrls: ['./tree-node-insert-dialog.component.css']
})
export class TreeNodeInsertDialogComponent {
  constructor(private dialogRef: MdDialogRef<TreeNodeInsertDialogComponent>) { }

  private insertTextNode(text) {
    const data = [];
    for (let i = 0; i < text.length; ++i) {
      data.push(text.charCodeAt(i));
    }
    this.dialogRef.close({
      data: data,
    });
  }
}
