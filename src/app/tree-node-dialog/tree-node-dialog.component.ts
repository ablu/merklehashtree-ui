import { Component, OnInit } from '@angular/core';
import {MdDialogRef} from '@angular/material';
import {Tree, TreeService} from '../tree.service';

@Component({
  selector: 'app-tree-node-dialog',
  templateUrl: './tree-node-dialog.component.html',
  styleUrls: ['./tree-node-dialog.component.css'],
})
export class TreeNodeDialogComponent implements OnInit{
  public node: Tree;
  private textValue = '';

  constructor(private dialogRef: MdDialogRef<TreeNodeDialogComponent>, private service: TreeService) {
  }

  ngOnInit() {
    this.textValue = String.fromCharCode.apply(null, this.node.data);
  }

  private deleteNode() {
    this.service.deleteNode(this.node.dataHashValue);
  }

}
