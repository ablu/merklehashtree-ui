import { Component, OnInit } from '@angular/core';
import {Tree, TreeService} from '../tree.service';

import * as SigmaJs from 'sigma';
import Sigma = SigmaJs.Sigma;
import Graph = SigmaJs.Graph;
import Node = SigmaJs.Node;
import {MdDialog, MdDialogRef} from '@angular/material';
import {TreeNodeDialogComponent} from '../tree-node-dialog/tree-node-dialog.component';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css'],
})
export class TreeComponent implements OnInit {
  private sigma: Sigma;
  private graph: Graph;

  constructor(private service: TreeService, private dialog: MdDialog) {
    this.service.treeUpdated$.subscribe(() => {
      this.updateTree(this.service.tree);
    });
  }

  ngOnInit() {
    this.sigma = new SigmaJs({
      container: 'container',
      settings: {
        labelThreshold: 0,
      },
    });
    this.sigma.bind('clickNode', ev => this.onClick(ev));
    this.graph = this.sigma.graph;

    if (this.service.tree) {
      this.updateTree(this.service.tree);
    }
  }

  onClick(ev) {
    const node: Node = ev.data.node;
    const dialogRef: MdDialogRef<TreeNodeDialogComponent> = this.dialog.open(TreeNodeDialogComponent);
    dialogRef.componentInstance.node = this.service.nodes.filter(x => x.hashValue === node.id)[0];
  }

  insertTreeLevel(tree: Tree, leftBorder: number, rightBorder: number, y: number) {
    if (!tree) {
      return;
    }

    const center = (rightBorder - leftBorder) / 2 + leftBorder;
    this.graph.addNode({
      id: tree.hashValue,
      label: tree.hashValue.substr(0, 8),
      x: center,
      y: y,
      size: 1,
      color: '#000',
    });

    if (tree.left) {
      this.insertTreeLevel(tree.left, leftBorder, center, y + 1);
      this.graph.addEdge({
        id: 'e_' + tree.hashValue + '_' + tree.left.hashValue,
        source: tree.hashValue,
        target: tree.left.hashValue,
      });
    }

    if (tree.right) {
      this.insertTreeLevel(tree.right, center, rightBorder, y + 1);
      this.graph.addEdge({
        id: 'e_' + tree.hashValue + '_' + tree.right.hashValue,
        source: tree.hashValue,
        target: tree.right.hashValue,
      });
    }
  }

  updateTree(tree: Tree) {
    this.sigma.graph.clear();
    this.insertTreeLevel(tree, -10, 10, 0);
    this.sigma.refresh();
  }

}
