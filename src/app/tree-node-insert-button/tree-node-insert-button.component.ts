import { Component, OnInit } from '@angular/core';
import {TreeService} from '../tree.service';
import {MdDialog} from '@angular/material';
import {TreeNodeInsertDialogComponent} from '../tree-node-insert-dialog/tree-node-insert-dialog.component';

@Component({
  selector: 'app-tree-node-insert-button',
  templateUrl: './tree-node-insert-button.component.html',
  styleUrls: ['./tree-node-insert-button.component.css']
})
export class TreeNodeInsertButtonComponent {

  constructor(private dialog: MdDialog, private service: TreeService) { }

  createNode() {
    this.dialog.open(TreeNodeInsertDialogComponent)
      .afterClosed()
      .subscribe(result => {
        this.service.insertNode(result.data);
      });
    console.log('create node');
  }
}
