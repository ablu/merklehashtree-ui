import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

export class Tree {
  left: Tree;
  right: Tree;
  hashValue: string;
  dataHashValue: string;
  data: number[];
}

@Injectable()
export class TreeService {
  private treeUpdatedSource = new Subject<void>();
  treeUpdated$ = this.treeUpdatedSource.asObservable();

  tree: Tree;
  nodes: Tree[];

  private ws: WebSocket;

  static toHexString(byteArray) {
    return byteArray.map(byte => {
      return ('0' + (byte & 0xFF).toString(16)).slice(-2);
    }).join('');
  }

  constructor() {
    this.initWebSocketConnection();
  }

  insertNode(data) {
    const message = {
      type: 'insertNode',
      data: data,
    };
    this.ws.send(JSON.stringify(message));
  }

  deleteNode(id) {
    const message = {
      type: 'deleteNode',
      nodeId: id,
    };
    this.ws.send(JSON.stringify(message));
  }

  private initWebSocketConnection() {
    if (this.ws && this.ws.readyState !== WebSocket.CLOSED) {
      return;
    }

    this.ws = new WebSocket('ws://' + location.hostname + ':5555');
    this.ws.onclose = ev => {
      setTimeout(() => this.initWebSocketConnection(), 1000);
    };
    this.ws.onerror = ev => {
      setTimeout(() => this.initWebSocketConnection(), 1000);
    };
    this.ws.onmessage = ev => {
      this.onMessage(ev);
    };
  }

  private onMessage(ev: MessageEvent) {
    const json: string = ev.data;
    const data = JSON.parse(json);

    switch (data.type) {
      case 'Tree':
        this.updateTree(data.tree);
        break;
    }
  }

  private readTree(tree): Tree {
    const newTree = new Tree();
    newTree.hashValue = TreeService.toHexString(tree.hash);
    newTree.dataHashValue = TreeService.toHexString(tree.dataHash);
    newTree.data = tree.data;

    if (tree.left) {
      newTree.left = this.readTree(tree.left);
    }
    if (tree.right) {
      newTree.right = this.readTree(tree.right);
    }

    this.nodes.push(newTree);

    return newTree;
  }

  private updateTree(tree) {
    if (!tree) {
      this.tree = null;
      this.nodes = [];
    } else {
      this.nodes = [];
      this.tree = this.readTree(tree);
    }

    this.treeUpdatedSource.next();
  }
}
