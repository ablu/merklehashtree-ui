import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TreeComponent } from './tree/tree.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import 'hammerjs';
import {MdButtonModule, MdDialogModule, MdIconModule, MdInputModule, MdToolbarModule} from '@angular/material';
import { TreeNodeDialogComponent } from './tree-node-dialog/tree-node-dialog.component';
import {TreeService} from './tree.service';
import { TreeNodeInsertButtonComponent } from './tree-node-insert-button/tree-node-insert-button.component';
import { TreeNodeInsertDialogComponent } from './tree-node-insert-dialog/tree-node-insert-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    TreeComponent,
    TreeNodeDialogComponent,
    TreeNodeInsertButtonComponent,
    TreeNodeInsertDialogComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    MdIconModule,
    MdToolbarModule,
    MdDialogModule,
    MdButtonModule,
    MdInputModule
  ],
  providers: [
    TreeService,
  ],
  entryComponents: [
    TreeNodeDialogComponent,
    TreeNodeInsertDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
